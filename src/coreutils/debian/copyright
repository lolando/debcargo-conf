Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: coreutils
Upstream-Contact: uutils developers
Source: https://github.com/uutils/coreutils

Files: *
Copyright: 2018-2021 uutils developers
License: MIT

Files: tests/fixtures/dircolors/internal.expected src/uu/dircolors/src/colors.rs
Copyright: 1996-2016 Free Software Foundation, Inc.
License: permissive
 Copying and distribution of this file, with or without modification,
 are permitted provided the copyright notice and this notice are preserved.

Files: util/test-repo-whitespace.BAT
Copyright: 2016-2020 Roy Ivy III <rivy.dev@gmail.com>
License: MIT or Apache-2.0

Files: debian/*
Copyright:
 2020 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2020 Sylvestre Ledru <sylvestre@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Apache-2.0
 On Debian systems, the full text of the Apache License Version 2.0
 can be found in the file `/usr/share/common-licenses/Apache-2.0'.
