commit e7829a2b82fd4c5a82ea9e7dd6c78cc6ce7ffd75
Author: Jelmer Vernooĳ <jelmer@jelmer.uk>
Date:   Thu Aug 10 20:22:25 2023 +0100

    Port examples to latest versions

diff --git a/examples/count.rs b/examples/count.rs
index fdccf60..3e4eea3 100644
--- a/examples/count.rs
+++ b/examples/count.rs
@@ -56,12 +56,12 @@ extern crate prettytable;
 extern crate rayon;
 extern crate time;
 
-use prettytable::Table;
 use prettytable::format::consts;
+use prettytable::Table;
 use rayon::prelude::*;
-use std::ops::Range;
 use std::cmp;
-use time::{Duration, PreciseTime};
+use std::ops::Range;
+use time::{Duration, Instant};
 
 use core::card::*;
 use core::deck::cards;
@@ -91,7 +91,7 @@ struct Count {
 }
 
 fn count_null_supersets(deal_size: usize) -> Count {
-    let start_time = PreciseTime::now();
+    let start_time = Instant::now();
     let sum = (deal_size - 1..81)
         .into_par_iter()
         .map(|x| deal_hands(x, deal_size))
@@ -100,7 +100,7 @@ fn count_null_supersets(deal_size: usize) -> Count {
     Count {
         no_supersets: sum,
         combinations: choose(81, deal_size as u64),
-        time: start_time.to(PreciseTime::now()),
+        time: start_time.elapsed(),
     }
 }
 
@@ -176,9 +176,9 @@ fn generate_table() {
 ////////////////////////////////////////////////////////////////////////////////
 
 fn main() {
-    clap_app!(count =>
-              (version: VERSION)
-              (about: "Finds all n-card deals that contain no SuperSets."))
+    clap::Command::new("count")
+        .version(VERSION)
+        .about("Finds all n-card deals that contain no SuperSets.")
         .get_matches();
 
     // initialize lookup table
@@ -223,7 +223,7 @@ fn build_lookup() {
 macro_rules! lookup {
     ($a:ident, $b:ident) => {
         SETS.get_unchecked($a).get_unchecked($b);
-    }
+    };
 }
 
 fn is_superset(a: usize, b: usize, c: usize, d: usize) -> bool {
diff --git a/examples/genpng.rs b/examples/genpng.rs
index fc6ff36..92ca634 100644
--- a/examples/genpng.rs
+++ b/examples/genpng.rs
@@ -23,7 +23,7 @@ extern crate core;
 use cairo::{Context, Format, ImageSurface, Operator, Rectangle};
 use std::f64::consts::FRAC_PI_2;
 use std::fs::File;
-use std::{io, mem};
+use std::mem;
 
 use core::deck::cards;
 use core::graphics::*;
@@ -32,17 +32,21 @@ use core::utils::clamp;
 const VERSION: &str = env!("CARGO_PKG_VERSION");
 const CARD_ASPECT_RATIO: f64 = 3.5 / 2.25;
 
-fn generate_card_images(path: &str, card_width: i32, border: i32,
-                        vertical: bool, scheme: ColorScheme) -> io::Result<()>
-{
+fn generate_card_images(
+    path: &str,
+    card_width: i32,
+    border: i32,
+    vertical: bool,
+    scheme: ColorScheme,
+) -> std::result::Result<(), Box<dyn std::error::Error>> {
     let card_height = (card_width as f64 / CARD_ASPECT_RATIO).ceil() as i32;
-    let card_rect = Rectangle {
-        // offset by (border, border)
-        x: border as f64,
-        y: border as f64,
-        width: card_width as f64,
-        height: card_height as f64,
-    };
+    // offset by (border, border)
+    let card_rect = Rectangle::new(
+        border as f64,
+        border as f64,
+        card_width as f64,
+        card_height as f64,
+    );
 
     // add space for the border on each edge
     let mut ctx_width = card_width + border * 2;
@@ -54,7 +58,7 @@ fn generate_card_images(path: &str, card_width: i32, border: i32,
     // create the surface and context
     let surface = ImageSurface::create(Format::ARgb32, ctx_width, ctx_height)
         .expect("Could not create surface.");
-    let ctx = Context::new(&surface);
+    let ctx = Context::new(&surface)?;
     if vertical {
         // adjust the transform to account for the vertical orientation
         ctx.rotate(FRAC_PI_2);
@@ -65,25 +69,26 @@ fn generate_card_images(path: &str, card_width: i32, border: i32,
         // completely clear the context to avoid accumulating color on
         // any edge that antialiases over the transparent background
         // (e.g. rounded card corners)
-        ctx.save();
+        ctx.save()?;
         ctx.set_operator(Operator::Clear);
-        ctx.paint();
-        ctx.restore();
+        ctx.paint()?;
+        ctx.restore()?;
 
         if border > 0 {
             ctx.rounded_rect(card_rect, card_corner_radius(card_rect));
             ctx.set_source_gray(0.0);
             // half the stroke will be covered by the card
             ctx.set_line_width(border as f64 * 2.);
-            ctx.stroke();
+            ctx.stroke()?;
         }
 
-        ctx.draw_card(card, card_rect, None, scheme);
+        ctx.draw_card(card, card_rect, None, scheme)?;
 
         let filename = format!("{}/{}.png", path, card.index());
         let mut image = File::create(&filename)?;
 
-        surface.write_to_png(&mut image)
+        surface
+            .write_to_png(&mut image)
             .unwrap_or_else(|_| println!("Error writing {}", filename));
     }
 
@@ -91,27 +96,34 @@ fn generate_card_images(path: &str, card_width: i32, border: i32,
 }
 
 fn main() {
-    let matches = clap_app!(genpng =>
-        (version: VERSION)
-        (about: "Generate an image for each Marmoset card.")
-        (@arg DIRECTORY: +required "Sets the directory in which to place the images")
-        (@arg VERTICAL: -v --("render-vertically") "Orients cards vertically")
-        (@arg CLASSIC: -c --("classic-colors") "Uses classic SET colors")
-        (@arg BORDER: -b --border +takes_value "Sets the border width in pixels")
-        (@arg WIDTH: -w --width +takes_value "Sets the card width in pixels")
-    ).get_matches();
-
-    let path = matches.value_of("DIRECTORY").unwrap();
-    let width = value_t!(matches, "WIDTH", i32).unwrap_or(350);
-    let border = value_t!(matches, "BORDER", i32).unwrap_or(0);
-    let render_vertically = matches.is_present("VERTICAL");
-    let classic_colors = matches.is_present("CLASSIC");
+    let matches = clap::Command::new("genpng")
+        .version(VERSION)
+        .about("Generate an image for each Marmoset card.")
+        .args([
+            clap::arg!(--directory <DIRECTORY> "Sets the directory in which to place the images")
+                .required(true),
+            clap::arg!(-v --"render-vertically" "Orients cards vertically"),
+            clap::arg!(-c --"classic-colors" "Uses classic SET colors"),
+            clap::arg!(-b --border <BORDER> "Sets the border width in pixels"),
+            clap::arg!(-w --width <WIDTH> "Sets the card width in pixels"),
+        ])
+        .get_matches();
+
+    let path = matches.get_one::<String>("DIRECTORY").unwrap();
+    let width = *matches.get_one::<i32>("WIDTH").unwrap_or(&350 as &i32);
+    let border = *matches.get_one::<i32>("BORDER").unwrap_or(&0 as &i32);
+    let render_vertically = matches.get_flag("VERTICAL");
+    let classic_colors = matches.get_flag("CLASSIC");
 
     // keep values within reasonable ranges
     let width = clamp(width, (64, 6400));
     let border = clamp(border, (0, 64));
-    let scheme = if classic_colors { ColorScheme::Classic } else { ColorScheme::CMYK };
+    let scheme = if classic_colors {
+        ColorScheme::Classic
+    } else {
+        ColorScheme::CMYK
+    };
 
-    generate_card_images(&path, width, border, render_vertically, scheme)
+    generate_card_images(path, width, border, render_vertically, scheme)
         .unwrap_or_else(|e| println!("{}", e));
 }
diff --git a/examples/simulate.rs b/examples/simulate.rs
index 6995379..5dec0b4 100644
--- a/examples/simulate.rs
+++ b/examples/simulate.rs
@@ -57,7 +57,7 @@ use rand::{thread_rng, Rng};
 use std::cmp;
 use std::sync::mpsc;
 use std::thread;
-use time::PreciseTime;
+use time::Instant;
 
 use core::card::*;
 use core::deck::cards;
@@ -281,7 +281,7 @@ fn simulate_game(counts: &mut Counts) {
 }
 
 fn run_simulations(num_games: u64, num_threads: u64) {
-    let start_time = PreciseTime::now();
+    let start_time = Instant::now();
     let (tx, rx) = mpsc::channel();
     let (thread_chunk, rem) = (num_games / num_threads, num_games % num_threads);
 
@@ -310,7 +310,7 @@ fn run_simulations(num_games: u64, num_threads: u64) {
     }
 
     // summary
-    println!("{} seconds elapsed.\n", start_time.to(PreciseTime::now()));
+    println!("{} seconds elapsed.\n", start_time.elapsed());
     totals.print_hand_stats();
     println!();
     totals.print_end_game_stats();
@@ -321,25 +321,25 @@ fn run_simulations(num_games: u64, num_threads: u64) {
 ////////////////////////////////////////////////////////////////////////////////
 
 fn main() {
-    let games_help = &format!(
-        "Sets number of games to simulate (default: {})",
-        pretty_print(NUM_GAMES)
-    );
-
-    let matches = clap_app!(simulate =>
-        (version: VERSION)
-        (about: "Gather statistics for simulated games of SET.")
-        (@arg GAMES: -g --games +takes_value games_help)
-        (@arg THREADS: -t --threads +takes_value "Sets number of threads")
-    )
-    .get_matches();
-
-    let num_games = value_t!(matches, "GAMES", u64).unwrap_or(NUM_GAMES);
-    let num_threads = value_t!(matches, "THREADS", u64).unwrap_or(num_cpus::get() as u64);
+    let games_default = pretty_print(NUM_GAMES).to_string();
+
+    let matches = clap::Command::new("simulate")
+        .version(VERSION)
+        .about("Gather statistics for simulated games of SET.")
+        .args([
+            clap::arg!(-g --games <GAMES> "Sets number of games to simulate"),
+            clap::arg!(-t --threads <THREADS> "Sets number of threads"),
+        ])
+        .get_matches();
+
+    let num_games = *matches.get_one::<u64>("GAMES").unwrap_or(&NUM_GAMES);
+    let num_threads = *matches
+        .get_one::<usize>("THREADS")
+        .unwrap_or(&num_cpus::get());
 
     println!(
         "Simulating {} games. This may take some time...",
         pretty_print(num_games)
     );
-    run_simulations(num_games, num_threads);
+    run_simulations(num_games, num_threads as u64);
 }
