Description: Rework code relying on unstable features
 Upstream makes uses of the new `let ... else` feature, only available
 since rust 1.64 (it was an unstable feature in previous versions).
 This one can be easily reworked so we get the same result in rust
 1.63.
Author: Arnaud Ferraris <aferraris@debian.org>
Forwarded: not-needed
Last-Update: 2023-02-08
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -109,27 +109,27 @@
 /// - If the destination path is not a valid C String
 /// - Or the swapoff function fails
 pub fn swapoff<P: AsRef<Path>>(dest: P) -> io::Result<()> {
-    let Ok(swap) = CString::new(dest.as_ref().as_os_str().as_bytes().to_owned()) else {
-        return Err(Error::new(
+    if let Ok(swap) = CString::new(dest.as_ref().as_os_str().as_bytes().to_owned()) {
+        match unsafe { c_swapoff(swap.as_ptr()) } {
+            0 => Ok(()),
+
+            _err => Err(Error::new(
+                ErrorKind::Other,
+                format!(
+                    "failed to swapoff {}: {}",
+                    dest.as_ref().display(),
+                    Error::last_os_error()
+                ),
+            )),
+        }
+    } else {
+        Err(Error::new(
             ErrorKind::Other,
             format!(
                 "swap path is not a valid c string: '{}'",
                 dest.as_ref().display()
             )
         ))
-    };
-
-    match unsafe { c_swapoff(swap.as_ptr()) } {
-        0 => Ok(()),
-
-        _err => Err(Error::new(
-            ErrorKind::Other,
-            format!(
-                "failed to swapoff {}: {}",
-                dest.as_ref().display(),
-                Error::last_os_error()
-            ),
-        )),
     }
 }
 
--- a/examples/umount.rs
+++ b/examples/umount.rs
@@ -22,10 +22,10 @@
         UnmountFlags::empty()
     };
 
-    let Err(why) = unmount(src, flags) else {
-        return ExitCode::SUCCESS;
-    };
-
-    eprintln!("failed to unmount {}: {}", src, why);
-    ExitCode::FAILURE
+    if let Err(why) = unmount(src, flags) {
+        eprintln!("failed to unmount {}: {}", src, why);
+        ExitCode::FAILURE
+    } else {
+        ExitCode::SUCCESS
+    }
 }
